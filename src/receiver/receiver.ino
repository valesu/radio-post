#include <Manchester.h>

/*
MAN_300 0
MAN_600 1
MAN_1200 2
MAN_2400 3
MAN_4800 4
MAN_9600 5
MAN_19200 6
MAN_38400 7

*/

#define RX_PIN 0
#define LED_PIN 4
#define TX_SPEED MAN_600

/*
Message format
MH
	7	6	5	4	3	2	1	0
	MT1	MT0	SW	X	X	V10	V9	V8

ML
	7	6	5	4	3	2	1	0
	V7	V6	V5	V4	V3	V2	V2	V0

MT1-MT0: Message Type
	MT1	MT0
	0	0	Init event
	0	1	Status message
X: Unused
SW: Switch state
V0-V10: Battery level
*/
#define MT1 15
#define MT0 14
#define SW 13
#define BATTERY_LEVEL_MASK 0x7FF

uint8_t moo = 1;

void setup() {
	Serial.begin(9600);

	pinMode(LED_PIN, OUTPUT);  
	digitalWrite(LED_PIN, moo);
	man.setupReceive(RX_PIN, TX_SPEED);
	man.beginReceive();
}

void loop() {
	if (man.receiveComplete()) {
		uint16_t m = man.getMessage();
		print_message(m);
		man.beginReceive(); //start listening for next message right after you retrieve the message
		moo = ++moo % 2;
		digitalWrite(LED_PIN, moo);
	}
}

void print_message(uint16_t msg) {
	Serial.print("Message: ");
	Serial.println(msg);
	
	byte mt = msg >> MT0;
	switch(mt) {
		case 0:
			Serial.print("Init: ");
			break;
		case 1:
			Serial.print("Status event: ");
			break;
		default:
			Serial.print("Unknown: ");
	}

	Serial.print("[");

	Serial.print("switch: ");
	uint16_t switch_state = msg & (_BV(SW));
	Serial.print(switch_state ? "on" : "off");
	
	Serial.print(", battery level: ");
	uint16_t battery_level = msg & BATTERY_LEVEL_MASK;
	Serial.print((float)battery_level / 100);
	Serial.print("V");

	Serial.println("]");
}