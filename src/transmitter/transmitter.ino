#include <avr/sleep.h>
#include <avr/wdt.h>
#include <Manchester.h>

/*
MAN_300 0
MAN_600 1
MAN_1200 2
MAN_2400 3
MAN_4800 4
MAN_9600 5
MAN_19200 6
MAN_38400 7
*/
#define TX_SPEED MAN_600
#define TX_PIN 1
#define TRANSMITTER_PIN 3

/*
State register
SRH
	7	6	5	4	3	2	1	0
	DNU	DNU	SW	X	X	V10	V9	V8

SRL
	7	6	5	4	3	2	1	0
	V7	V6	V5	V4	V3	V2	V2	V0

	DNU: Do not use
	X: Unused
	SW: Switch state
	V0-V10: Battery level
*/
#define SW 13
#define BATTERY_LEVEL_MASK 0x7FF

/*
Message format
	14-15 [MT0-MT1]: Message Type
	0-13: Message payload

	MT1	MT0
	0	0	Init event
	0	1	Status message
*/
#define MT1 15
#define MT0 14
#define MESSAGE_TYPE_MASK 0xC000
#define MESSAGE_PAYLOAD_MASK 0x3FFF

// Switch pin is INT0 interrupt pin PB2 = arduino pin 2
#define SWITCH_PIN 2

// 0=16ms, 1=32ms, 2=64ms, 3=128ms, 4=250ms, 5=500ms
// 6=1sec, 7=2sec, 8=4sec, 9=8sec
#define WDT_PRESCALER 9

// With highest prescaler value 9 = 8sec
// 1 = once per 8 seconds
// 2 = once per 16 seconds
// 900 = (2 * 60 * 60) / 8 = once per two hours
#define WDT_INTERRUPT_PERIOD 8

// Internal Vref is 1.1V. This should be corrected though since it is individual to the controller
#define INTERNAL_V_REF 1.1

volatile int wdt_counter = WDT_INTERRUPT_PERIOD;
volatile boolean switch_event = 0;
uint16_t state = 0;

void setup() {
	setup_transmitter();
	setup_switch();
	setup_watchdog(WDT_PRESCALER);

	update_state();
	send_init_message();
}

void setup_transmitter() {
	// Workaround for 1MHz AtTiny85
	man.workAround1MhzTinyCore();
	man.setupTransmit(TX_PIN, TX_SPEED);
}

void setup_switch() {
	pinMode(SWITCH_PIN, INPUT);
	enable_int0_interrupt();
}

void enable_int0_interrupt() {
	// Clear INT0 sense control bits
	MCUSR &= ~(_BV(ISC01) | _BV(ISC00));

	// Enable INT0 interrupt
	GIMSK |= _BV(INT0);
}

void disable_int0_interrupt() {
	// Disable INT0 interrupt
	GIMSK &= ~_BV(INT0);
}

void setup_watchdog(int timerPrescaler) {
	if (timerPrescaler > 9 ) timerPrescaler = 9;

	byte bb = timerPrescaler & 7;
	//Set the special 5th bit if necessary
	if (timerPrescaler > 7) bb |= _BV(5);

	//Clear the watch dog reset
	MCUSR &= ~_BV(WDRF);
	//Set WD_change enable, set WD enable
	WDTCR |= _BV(WDCE) | _BV(WDE);
	//Set new watchdog timeout value
	WDTCR = bb;
	//Set the interrupt enable, this will keep unit from resetting after each int
	WDTCR |= _BV(WDIE);
}

void loop() {
	// Rearm INT0 if WDT interrupt and switch is off (HIGH)
	if (!switch_event && digitalRead(SWITCH_PIN)) {
		enable_int0_interrupt();
	}

	// Process events
	if (switch_event || wdt_counter <= 0) {
		update_state();
		send_status_message();

		switch_event = 0;
		wdt_counter = WDT_INTERRUPT_PERIOD;
	}

	power_down();
}

void send_init_message() {
	transmit_message(0);
}

void send_status_message() {
	transmit_message(_BV(MT0));
}

void transmit_message(uint16_t msg_type) {
	set_transmitter_on();
	man.transmit(msg_type & MESSAGE_TYPE_MASK | state & MESSAGE_PAYLOAD_MASK);
	set_transmitter_off();
}

void update_state() {
	state = 0;

	// Set switch state
	state |= digitalRead(SWITCH_PIN) ? 0 : 1 << SW;

	// Set battery level
	state |= get_battery_level() & BATTERY_LEVEL_MASK;
}

void set_transmitter_on() {
	// Turn transmitter on
	pinMode(TRANSMITTER_PIN, OUTPUT);
	digitalWrite(TRANSMITTER_PIN, HIGH);
}

void set_transmitter_off() {
	// Turn transmitter off
	digitalWrite(TRANSMITTER_PIN, LOW);
	pinMode(TRANSMITTER_PIN, INPUT);
}

uint16_t get_battery_level() {
	// Switch ADC ON
	ADCSRA |= _BV(ADEN);

	// Set reference to Vcc and measurement to the internal 1.1Vref
	ADMUX = _BV(MUX3) | _BV(MUX2);
	delay(2);

	// Start conversion
	ADCSRA |= _BV(ADSC);

	// Wait for results
	while (bit_is_set(ADCSRA, ADSC));

 	// The order is important here. Reading ADCL first locks ADCH. Reading ADCH unlocks both
 	uint8_t low  = ADCL;
 	uint8_t high = ADCH;
 	uint16_t result = high << 8 | low;

 	result = INTERNAL_V_REF * 1023 * 100 / result;

	// Switch ADC OFF
	ADCSRA |= _BV(ADEN);

	return result;
}

void power_down() {
	prepare_for_sleep();

	sleep_enable();

	// Software BOD disable is only available for attiny85 rev C and newer
	// Since I have rev B, there is no point in disabling it programmatically. Will use fuses
	// sleep_bod_disable();

	sleep_cpu();
	sleep_disable();
}

void prepare_for_sleep() {
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);

	// Switch ADC OFF
	ADCSRA &= ~_BV(ADEN);
}

ISR(WDT_vect) {
	wdt_counter--;
}

ISR(INT0_vect) {
	disable_int0_interrupt();
	switch_event = 1;
}